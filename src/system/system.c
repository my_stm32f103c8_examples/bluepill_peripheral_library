#include <stm32f1xx_hal.h>

uint16_t gpio_a_ussage_count = 0;

void SysTick_Handler(void)
{
    HAL_IncTick();
}

void NMI_Handler(void)
{

}

void HardFault_Handler(void)
{
    while(1);
}

void MemManage_Handler(void)
{
    while(1);
}

void BusFault_Handler(void)
{
    while(1);
}

void UsageFault_Handler(void)
{
    while(1);
}

void SVC_Handler(void)
{

}

void DebugMon_Handler(void)
{

}

void PendSV_Handler(void)
{

}

void default_error_handler(void)
{
    __disable_irq();
    while(1);
}

void set_system_clock_config(void)
{
    RCC_OscInitTypeDef osc_init_config  = {0};
    RCC_ClkInitTypeDef clk_init_config  = {0};
    
    
    osc_init_config.OscillatorType  = RCC_OSCILLATORTYPE_HSE;
    osc_init_config.HSEState        = RCC_HSE_ON;
    osc_init_config.HSEPredivValue  = RCC_HSE_PREDIV_DIV1;
    osc_init_config.HSIState        = RCC_HSI_ON;
    osc_init_config.PLL.PLLState    = RCC_PLL_ON;
    osc_init_config.PLL.PLLSource   = RCC_PLLSOURCE_HSE;
    osc_init_config.PLL.PLLMUL      = RCC_PLL_MUL9;
    
    if (HAL_RCC_OscConfig(&osc_init_config) != HAL_OK)
    {
        default_error_handler();
    }
    
    clk_init_config.ClockType       = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    clk_init_config.SYSCLKSource    = RCC_SYSCLKSOURCE_PLLCLK;
    clk_init_config.AHBCLKDivider   = RCC_SYSCLK_DIV1;
    clk_init_config.APB1CLKDivider  = RCC_HCLK_DIV2;
    clk_init_config.APB2CLKDivider  = RCC_HCLK_DIV1;
    
    if (HAL_RCC_ClockConfig(&clk_init_config, FLASH_LATENCY_2) != HAL_OK)
    {
        default_error_handler();
    }
    HAL_RCC_EnableCSS();    
}