#ifndef SPI_H
#define SPI_H

#include <stdint.h>

void spi_init(void);
void spi_deinit(void);
uint16_t spi_write(uint8_t* buffer, uint16_t length);
uint16_t spi_read(uint8_t* buffer, uint16_t length);

#endif