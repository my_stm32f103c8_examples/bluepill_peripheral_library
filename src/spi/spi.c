#include "stm32f1xx_hal.h"
#include "system/system.h"

extern uint16_t gpio_a_ussage_count;

#define SPI                 SPI1
#define SPI_PORT            GPIOA
#define SPI_MISO_PIN        GPIO_PIN_6
#define SPI_MOSI_PIN        GPIO_PIN_7
#define SPI_SCK_PIN         GPIO_PIN_5
#define SPI_MODE            SPI_MODE_MASTER
#define SPI_CRC_POLYNOMIAL  10

SPI_HandleTypeDef   spi_handle  = {0};

void spi_init(void)
{
    spi_handle.Instance                 = SPI;
    spi_handle.Init.Mode                = SPI_MODE;
    spi_handle.Init.Direction           = SPI_DIRECTION_2LINES;
    spi_handle.Init.DataSize            = SPI_DATASIZE_8BIT;
    spi_handle.Init.CLKPolarity         = SPI_POLARITY_LOW;
    spi_handle.Init.CLKPhase            = SPI_PHASE_1EDGE;
    spi_handle.Init.NSS                 = SPI_NSS_SOFT;
    spi_handle.Init.BaudRatePrescaler   = SPI_BAUDRATEPRESCALER_4;
    spi_handle.Init.FirstBit            = SPI_FIRSTBIT_MSB;
    spi_handle.Init.TIMode              = SPI_TIMODE_DISABLE;
    spi_handle.Init.CRCCalculation      = SPI_CRCCALCULATION_DISABLE;
    spi_handle.Init.CRCPolynomial       = SPI_CRC_POLYNOMIAL;

    if (HAL_SPI_Init(&spi_handle) != HAL_OK)
    {
        default_error_handler();
    }
}

void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{
    GPIO_InitTypeDef    gpio_config = {0};

    __HAL_RCC_SPI1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    gpio_a_ussage_count++;

    gpio_config.Pin     = SPI_MOSI_PIN | SPI_SCK_PIN;
    gpio_config.Mode    = GPIO_MODE_AF_PP;
    gpio_config.Speed   = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(SPI_PORT, &gpio_config);

    gpio_config.Pin     = SPI_MISO_PIN;
    gpio_config.Mode    = GPIO_MODE_INPUT;
    gpio_config.Pull    = GPIO_NOPULL;
    HAL_GPIO_Init(SPI_PORT, &gpio_config);
}

void spi_deinit(void)
{
    HAL_SPI_DeInit(&spi_handle);
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{
    gpio_a_ussage_count--;

    if (gpio_a_ussage_count == 0)
    {
        __HAL_RCC_GPIOA_CLK_DISABLE();
    }

    __HAL_RCC_SPI1_CLK_DISABLE();
    HAL_GPIO_DeInit(SPI_PORT, SPI_MOSI_PIN); 
    HAL_GPIO_DeInit(SPI_PORT, SPI_MISO_PIN); 
    HAL_GPIO_DeInit(SPI_PORT, SPI_SCK_PIN); 
}