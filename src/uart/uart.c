#include "stm32f1xx_hal.h"
#include "system/system.h"
#include "uart.h"

extern uint16_t gpio_a_ussage_count;

#define UART_PORT       GPIOA
#define UART_RX_PIN     GPIO_PIN_10
#define UART_TX_PIN     GPIO_PIN_9
#define UART            USART1
#define UART_BAUDRATE   115200
#define UART_TIMEOUT    10

UART_TransferType transfer_mode;
UART_HandleTypeDef uart_handle = {0};

void uart_init(UART_TransferType transfer_type)
{
    transfer_mode = transfer_type;

    uart_handle.Instance            = UART;
    uart_handle.Init.BaudRate       = UART_BAUDRATE;
    uart_handle.Init.WordLength     = USART_WORDLENGTH_8B;
    uart_handle.Init.StopBits       = UART_STOPBITS_1;
    uart_handle.Init.Parity         = UART_PARITY_NONE;
    uart_handle.Init.Mode           = UART_MODE_TX;
    uart_handle.Init.HwFlowCtl      = UART_HWCONTROL_NONE;
    uart_handle.Init.OverSampling   = UART_OVERSAMPLING_16;

    if (HAL_UART_Init(&uart_handle) != HAL_OK)
    {
        default_error_handler();
    }
}

void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
    GPIO_InitTypeDef uart_gpio = {0};

    __HAL_RCC_USART1_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    gpio_a_ussage_count++;

    // initialize uart Rx pin
    uart_gpio.Pin       = UART_RX_PIN;
    uart_gpio.Mode      = GPIO_MODE_INPUT;
    uart_gpio.Pull      = GPIO_NOPULL;
    HAL_GPIO_Init(UART_PORT, &uart_gpio);

    // initialize uart Rx pin
    uart_gpio.Pin       = UART_TX_PIN;
    uart_gpio.Mode      = GPIO_MODE_AF_PP;
    uart_gpio.Speed     = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(UART_PORT, &uart_gpio);

    if (transfer_mode == INTERRUPT_TRANSFER)
    {
        HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
    }
}

void uart_deinit(void)
{
    HAL_UART_DeInit(&uart_handle);
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{
    gpio_a_ussage_count--;

    if (gpio_a_ussage_count == 0)
    {
        __HAL_RCC_GPIOA_CLK_DISABLE();
    }

    if (transfer_mode == INTERRUPT_TRANSFER)
    {
        HAL_NVIC_DisableIRQ(USART1_IRQn);
    }
    
    __HAL_RCC_USART1_CLK_DISABLE();
    HAL_GPIO_DeInit(UART_PORT, UART_RX_PIN);
    HAL_GPIO_DeInit(UART_PORT, UART_TX_PIN);
}

uint32_t uart_read(uint8_t* buffer, uint16_t buffer_size)
{
    HAL_StatusTypeDef ret = HAL_OK;
    switch (transfer_mode)
    {
        case NORMAL_TRANSFER:
            ret = HAL_UART_Receive(&uart_handle, buffer, buffer_size, UART_TIMEOUT);
            break;
        case INTERRUPT_TRANSFER:
            ret = HAL_UART_Receive_IT(&uart_handle, buffer, buffer_size);
            break;
    }

    if (ret == HAL_OK)
    {
        return buffer_size;
    }
    else
    {
        return -1;
    }
}

uint32_t uart_write(uint8_t* buffer, uint16_t buffer_size)
{
    HAL_StatusTypeDef ret = HAL_OK;
    switch (transfer_mode)
    {
        case NORMAL_TRANSFER:
            ret = HAL_UART_Transmit(&uart_handle, buffer, buffer_size, UART_TIMEOUT);
            break;
        case INTERRUPT_TRANSFER:
            ret = HAL_UART_Transmit_IT(&uart_handle, buffer, buffer_size);
            break;
    }
    if (ret == HAL_OK)
    {
        return buffer_size;
    }
    else
    {
        return -1;
    }
}

void USART1_IRQHandler(void)
{
    HAL_UART_IRQHandler(&uart_handle);
}