#ifndef UART_H
#define UART_H

typedef enum uart_transfer_type {
    NORMAL_TRANSFER = 0x00,
    INTERRUPT_TRANSFER,
    DMA_TRANSFER, 
}UART_TransferType;

void uart_init(UART_TransferType transfer_type);
void uart_deinit(void);
uint32_t uart_read(uint8_t* buffer, uint16_t buffer_size);
uint32_t uart_write(uint8_t* buffer, uint16_t buffer_size);

#endif