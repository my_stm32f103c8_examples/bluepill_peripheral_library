#include <stm32f1xx_hal.h>
#include "system/system.h"
#include "uart/uart.h"

int main(void)
{
    uint8_t data[]  = "Hello world...!\r\n";
    uint8_t number_of_iterration    = 10;

    HAL_Init();
    set_system_clock_config();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    uart_init(INTERRUPT_TRANSFER);

    while (number_of_iterration > 0)
    {
        uart_write(data, 18);
        HAL_Delay(1); 
        number_of_iterration--;
    }

    uart_deinit(); 

    while (1);
    return 0;
}